<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_label.contains_polls',
    1 => 'polls',
    2 => 'apps-pagetree-folder-contains-polls'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-polls'] = 'apps-pagetree-folder-contains-polls';
