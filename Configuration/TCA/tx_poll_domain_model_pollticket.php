<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollticket';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

return [
    'ctrl' => [
        'title' => $lll,
        'label' => 'poll',
        'label_alt' => 'frontend_user',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:poll/Resources/Public/Icons/iconmonstr-language-10.svg'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, poll, frontend_user, poll_ticket_answer, is_finished',
    ],
    'types' => [
        '1' => ['showitem' => 'poll, frontend_user, poll_ticket_answer, is_finished'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::full('endtime'),
        'frontend_user' => [
            'exclude' => 0,
            'label' => $lll . '.frontend_user',
            'displayCond' => 'FIELD:frontend_user:>:0',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'fe_users',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 0,
                'readOnly' => 1,
                'default' => '0',
            ],
        ],
        'is_finished' => [
            'exclude' => 1,
            'label' => $lll . '.is_finished',
            'config' => \CodingMs\Poll\Tca\ConfigurationPresetCustom::get('checkbox'),
        ],
        'poll' => [
            'exclude' => 0,
            'label' => $lll . '.poll',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_poll',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
        'poll_ticket_answer' => [
            'exclude' => 0,
            'label' => $lll . '.poll_ticket_answer',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_poll_domain_model_pollticketanswer',
                'foreign_field' => 'poll_ticket',
                'maxitems' => 9999,
                'appearance' => [
                    'collapse' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'enabledControls' => [
                        'info' => false,
                        'new' => false,
                        'dragdrop' => false,
                        'sort' => false,
                        'hide' => false,
                        'delete' => false
                    ],
                ],
            ],
        ],
    ],
];
