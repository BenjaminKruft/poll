# Data records/model

## Poll slips

"Survey slips" are data records that are saved for each complete participation in a poll.

"Survey slips" can be deactivated in the plugin settings. The option is activated by default.

If you use user-defined answers (free text fields) this option must be activated.

If this option is deactivated, answers are saved in answer data records. This gives a slightly better performance. In addition, the survey can be modified while it is running without making the export function unusable by changing the data structure.

If this option is activated, surveys begun by logged-in frontend users can be continued at a later time.

## Poll data records/Poll model

This data record contains a poll which can contain multiple questions.

Polls have the following settings:

Name                       |Description
---------------------------|-------------------------------------------------------------------------------------------
Language                   |Set the poll language here.
Hide                       |Deactivate the poll here.
Poll title                 |Enter the poll title here.
Description                |Enter an introductory text for the poll here.
Poll questions             |Enter questions for the poll here, of type Poll Question Model


## Poll Question Data Record/Poll Question Model

This data record contains the questions in the poll. A question can have different question types.

Questions have the following settings:

Name                        |Description
----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Question                    |Enter the poll question here.
Position                    |The position indicates the order in which the questions are displayed (if you have defined more than one question). The order can easily be changed via drag and drop on the gray title bar of the dataset.
Question type               |The question responds differently depending on the question type.
Answers                     |Possible answers to the question are entered here - these are of the type Poll Question Answer Model

### Question types
Name                  |Description
----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Single                | This question type has radio buttons, so only one answer is possible.
Multiple              | This question type has checkboxes, so multiple answers are possible.
SingleUserAnswer      | This question type has a text field, so a free text answer is possible.
MultipleUserAnswer    | This question type has several text fields, so several free text answers are possible.

## Poll Question Answer Data Record / Poll Question Answer Model

This record contains a possible answer to a question. Questions can have multiple answers.

Answers have the following settings:

Name                                        |Description
--------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Answer                                      |Enter the answer here.
Position                                    |The position indicates the order in which the questions are displayed. The order can easily be changed via drag and drop on the gray title bar of the dataset.
Participants can enter an answer themselves |Set whether a user can enter an answer themselves.
Placeholder for answer text field           |Enter the placeholder for the text field of the free text answer.

## Poll ticket record / Poll ticket model

This record contains saved responses to a poll. A poll ticket is created for each participant.

**Note:** A poll ticket cannot be deleted.

Poll tickets have the following settings:

Name                                        |Description
--------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Answer                                      |Enter the answer here.
Position                                    |The position indicates the order in which the questions are displayed. The order can easily be changed via drag and drop on the gray title bar of the dataset.
Participants can enter an answer themselves |Set whether a user can enter an answer themselves.
Placeholder for answer text field           |Enter the placeholder for the text field of the free text answer.
