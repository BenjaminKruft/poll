# Installation

Zur Installation der Umfrage-Extension führen Sie die folgenden Schritte durch:

* Installieren Sie die Extension über den Extension-Manager oder
  mittels composer (`composer req codingms/poll`).

* Öffnen Sie Ihr Haupt-Template und fügen Sie unter *Includes → Include static* den Eintrag `Poll` hinzu.

* Erstellen Sie in Ihrem TYPO3-Seitenbaum einen Container `Umfragen`. In diesem Verzeichnis
  legen Sie Ihre Umfragen an. Dazu verwenden Sie das "Liste"-Modul und erstellen Sie einen neuen Datensatz
  vom Typ "Umfrage". Legen Sie am besten nun eine Umfrage an.

* Diese "Container-Seite" muss der Erweiterung bekannt gemacht werden. Wechseln Sie dazu in den
  Konstanten-Editor und tragen Sie die ID der Seite unter "Poll" --> "Container für Umfrage-Datensätze" ein.

* Erstellen Sie nun auf Ihrer Webseite eine Seite, auf der Sie die Umfrage durchführen wollen.
  Fügen Sie auf der Seite ein Plugin vom Typ `Umfrage anzeigen` ein und wählen die zuvor angelegte
  Umfrage aus. Wenn Sie sich nun die Seite ansehen, sollte die Umfrage bereits angezeigt werden.
  Wenn Sie die Option "Ergebnis der Umfrage unter der Umfrage anzeigen" in den Plugin-Einstellungen
  aktivieren, wird das Umfrageergebnis bereits angezeigt, bevor der Nutzer selber an der
  Umfrage teilgenommen hat.

* Nun müssen wir noch eine Abschlussseite für die Umfrage definieren. Dies ist die Seite, auf die der Besucher
  umgeleitet wird, wenn er erfolgreich an der Umfrage teilgenommen hat. Dazu erstellen Sie eine
  Unterseite der aktuellen Umfrage-Seite. Auf dieser Seite platzieren Sie ein Plugin
  vom Typ `Umfrage-Ergebnis anzeigen`. Die ID dieser Seite tragen Sie wieder im Konstanten-Editor
  unter "Seite für Umfrageergebnis" ein.

* Optional können Sie nun noch auf Ihrer Startseite o.ä. eine Plugin vom Typ
  `Umfrage-Teaser anzeigen` einfügen, um Besucher auf Ihre Umfrage aufmerksam
  zu machen, oder ein Plugin vom Typ `Umfrage-Liste anzeigen` platzieren,
  damit Ihre Besucher auch an älteren Umfragen teilnehmen können.


## Installation der PRO-Version

Für die Pro-Version benötigen Sie die Extension "modules", die Sie im TER unter
https://extensions.typo3.org/extension/modules oder auf
https://gitlab.com/codingms/typo3-public/modules/-/tags finden können.
Für TYPO3 8 verwenden Sie bitte die Version 3.2, ab TYPO3 9 die Version 4.0.

Die PRO-Version können Sie wie die Basis-Version über den Extension-Manager oder mittels composer
installieren (`composer req codingms/poll-pro`).


