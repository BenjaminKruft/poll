<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}
//
// Plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.poll',
    'Poll',
    ['Poll' => 'show'],
    ['Poll' => 'show']
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.poll',
    'PollTeaser',
    ['Poll' => 'teaser'],
    ['Poll' => 'teaser']
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.poll',
    'PollResult',
    ['Poll' => 'result'],
    ['Poll' => 'result']
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.poll',
    'PollList',
    ['Poll' => 'list'],
    ['Poll' => 'list']
);
//
// Page TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:poll/Configuration/PageTS/tsconfig.typoscript">'
);
