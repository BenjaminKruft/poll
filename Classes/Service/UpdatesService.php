<?php

namespace CodingMs\Poll\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Perform an Update-Check
 *
 * @package poll
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * ChangeLog Version: 1.1.0
 *
 * *    PhpDoc comments
 *
 */
class UpdatesService
{

    /**
     * CURL HTTP header data
     * @var array
     */
    public static $curlHttpHeader = [
        'Accept: application/json',
        'Accept-Encoding: gzip',
        'Accept-Language: de'
    ];

    /**
     * CURL user agent
     * @var array
     */
    public static $curlUserAgent = [
        'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Safari/534.45'
    ];

    /**
     * Check for Updates by CURL or file_get_contents
     *
     * @param string $extension Extension-Key
     * @param array $thisVersionParts Version number parts
     * @return boolean|string
     */
    public static function check($extension = 'poll', $thisVersionParts)
    {
        $updateMessage = false;
        $url = 'https://www.coding.ms/updates/' . implode('.', $thisVersionParts) . '/' . $extension . '.json';
        $key = GeneralUtility::underscoredToUpperCamelCase($extension);
        $prefix = 'tx_' . strtolower($key) . '_message.';
        // In case of an empty TYPO3 cache, the Version is possibly not available
        if (trim($thisVersionParts[0]) == '' && trim($thisVersionParts[1]) == '' && trim($thisVersionParts[2]) == '') {
            return $updateMessage;
        }
        // Get current version by CURL
        if (function_exists('curl_init')) {
            // Init curl request
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, self::$curlHttpHeader);
            curl_setopt($ch, CURLOPT_USERAGENT, self::$curlUserAgent);
            curl_setopt($ch, CURLOPT_REFERER, 'http://' . $_SERVER['HTTP_HOST']);
            $currentVersionJson = curl_exec($ch);
            curl_close($ch);
        } // Fall back with file get contents
        else {
            $context = ['http' => ['header' => 'Referer: http://' . $_SERVER['HTTP_HOST']]];
            $streamContext = stream_context_create($context);
            $currentVersionJson = @file_get_contents($url, false, $streamContext);
        }
        // Getting version failed?
        if (!$currentVersionJson) {
            return LocalizationUtility::translate($prefix . 'error_version_not_detected', $key);
        }
        // Parsing JSON failed?
        $currentVersionArray = json_decode($currentVersionJson, true);
        if (!$currentVersionArray) {
            return LocalizationUtility::translate($prefix . 'error_version_json_invalid', $key);
        }
        $currentVersionParts = explode('.', $currentVersionArray['version']);
        //
        // Major-Release is higher
        // Installed Version>Update-Check-Version
        if ($thisVersionParts[0] > $currentVersionParts[0]) {
            // That's impossible
        }
        // Major-Release lower
        // Installed Version<Update-Check-Version
        else {
            if ($thisVersionParts[0] < $currentVersionParts[0]) {
                $updateMessage = LocalizationUtility::translate($prefix . 'info_major_release_available', $key);
                $updateMessage .= '<br />';
            } // Otherwise just a minor release or a patch can be possible
            else {
                // Minor-Release higher
                // Installed Version>Update-Check-Version
                if ($thisVersionParts[1] > $currentVersionParts[1]) {
                    // That's impossible
                } else {
                    // Minor-Release lower
                    if ($thisVersionParts[1] < $currentVersionParts[1]) {
                        $updateMessage = LocalizationUtility::translate($prefix . 'info_minor_release_available', $key);
                        $updateMessage .= '<br />';
                    } else {
                        if ($thisVersionParts[2] < $currentVersionParts[2]) {
                            // Otherwise just a patch can be possible
                            $updateMessage = LocalizationUtility::translate($prefix . 'info_patch_available', $key);
                            $updateMessage .= '<br />';
                        }
                    }
                }
            }
        }
        if (trim($updateMessage) != '') {
            $updateMessage .= '<br />';
            $updateMessage .= LocalizationUtility::translate($prefix . 'warning_update_message', $key);
            $thisVersionString = $thisVersionParts[0] . '.' . $thisVersionParts[1] . '.' . $thisVersionParts[2];
            $updateMessage .= '(' . $thisVersionString . ' -> ' . $currentVersionArray['version'] . ')';
        }
        return $updateMessage;
    }

}